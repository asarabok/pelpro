# Peludna prognoza
---
Application for collecting and graphically displaying pollen forecast data. Data is collected by scraping from the pages https://stampar.hr/hr/peludna-prognoza-za-hrvatsku.

### Run app with Docker
---

Steps:
1. Clone project
    ```bash
    git clone https://github.com/asarabok/pelpro.git
    ```
2. Position to project root in terminal
    ```bash
    cd pelpro
    ```
3. Build docker container with `docker-compose`
    ```bash
    docker-compose build
    ```
4. Run Docker container
    ```bash
    docker-compose up -d
    ```
5. Run db migrations and insert initial data (cities, plants and measurements)
    ```
    docker exec pelpro_flask flask db upgrade
    docker exec pelpro_flask flask load_fixture City
    docker exec pelpro_flask flask load_fixture Plant
    docker exec pelpro_flask flask load_fixture Measurement
    ```

6. GUI and API can be accessed on
    ```
    http://localhost:5000
    ```
    GUI: `/`
    API: `/api/measurements`

### API endpoints
---

 - All measurements
    ```
    /api/measurements
    ```
 - Measurements for cities
    ```
    /api/measurements/city/{city_id}
    ```
 - Measurements for plants
    ```
    /api/measurements/plant/{plant_id}
    ```
 - Measurements per plant in city
    ```
    /api/measurements/city/{city_id}/{plant_id}
    ```

##### API filters
 - Number of last days to show (int)  `?days_delta={num_days}`

##### Example:
Show measurements for corylus in Zagreb for last 30 days:
```
http://localhost:5000/api/measurements/city/1/lijeska-corylus-sp?days_delta=30
```

### Extra
---
Manually run scraper
```
docker exec pelpro_flask flask scrape_measurements
```
